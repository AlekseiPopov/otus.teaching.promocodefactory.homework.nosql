﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class CustomerPreference
    {
        [BsonIgnore]
        public Guid CustomerId { get; set; }
        [BsonIgnore]
        public virtual Customer Customer { get; set; }

        public Guid PreferenceId { get; set; }
        [BsonIgnore]
        public virtual Preference Preference { get; set; }
    }
}