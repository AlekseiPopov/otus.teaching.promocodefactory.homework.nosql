﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class PromoCode
        : BaseEntity
    {
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public Guid PartnerId { get; set; }
        [BsonIgnore]
        public virtual Preference Preference { get; set; }
        [BsonIgnoreIfDefault]
        public Guid PreferenceId { get; set; }
        [BsonIgnoreIfNull]
        public virtual ICollection<PromoCodeCustomer> Customers { get; set; }
    }
}