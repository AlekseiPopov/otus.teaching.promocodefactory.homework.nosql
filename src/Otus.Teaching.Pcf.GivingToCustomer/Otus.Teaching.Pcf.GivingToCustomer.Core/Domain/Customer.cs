﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class Customer
        :BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [BsonIgnore]
        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }
        [BsonIgnoreIfNull]
        public virtual ICollection<CustomerPreference> Preferences { get; set; }
        [BsonIgnoreIfNull]
        public virtual ICollection<PromoCodeCustomer> PromoCodes { get; set; }
    }
}