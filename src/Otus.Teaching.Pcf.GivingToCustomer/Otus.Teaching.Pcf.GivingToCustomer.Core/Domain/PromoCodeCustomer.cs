﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class PromoCodeCustomer : BaseEntity
    {
        public Guid PromoCodeId { get; set; }
        [BsonIgnore]
        public virtual PromoCode PromoCode { get; set; }

        public Guid CustomerId { get; set; }
        [BsonIgnore]
        public virtual Customer Customer { get; set; }
    }
}
