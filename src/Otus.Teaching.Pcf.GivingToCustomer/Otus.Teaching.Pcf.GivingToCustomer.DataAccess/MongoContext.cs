﻿using System;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess
{
    public class MongoContext : IMongoContext
    {
        public string CustomerCollectionName { get; set; }
        public string PreferenceCollectionName { get; set; }
        public string PromoCodeCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IMongoContext
    {
        string CustomerCollectionName { get; set; }
        string PreferenceCollectionName { get; set; }
        string PromoCodeCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
