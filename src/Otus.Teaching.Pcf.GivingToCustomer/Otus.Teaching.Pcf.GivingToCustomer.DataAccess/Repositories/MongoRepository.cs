﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class MongoRepository<T> : IMongoRepository<T> where T : BaseEntity
    {
        private readonly IMongoCollection<T> _collection;
        IMongoCollection<Preference> _preferenceCollection;
        IMongoCollection<PromoCode> _promocodeCollection;

        public MongoRepository(IMongoContext settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            var array = typeof(T).ToString().Split('.');
            _collection = database.GetCollection<T>(array[array.Length - 1] + "s");

            _preferenceCollection = database.GetCollection<Preference>("Preferences");
            _promocodeCollection = database.GetCollection<PromoCode>("Promocodes");
        }

        public async Task AddAsync(T entity)
        {
            await _collection.InsertOneAsync(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq("_id", new ObjectId(entity.Id.ToBson()));
            await _collection.DeleteOneAsync(filter);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var list =  await _collection.Find(new FilterDefinitionBuilder<T>().Empty).ToListAsync();

            foreach (var entity in list)
            {
                if (entity is Customer customer)
                {
                    await LoadData(customer);
                }
            }
            return list;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity =  await _collection.Find<T>(e => e.Id == id).FirstOrDefaultAsync();
            if (entity is Customer customer)
            {
                await LoadData(customer);
            }

            return entity;
        }

        public async Task<T> GetFirstWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return await _collection.Find<T>(predicate).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var list =  await _collection.Find<T>(x => ids.Contains(x.Id)).ToListAsync();

            foreach (var entity in list)
            {
                if (entity is Customer customer)
                {
                    await LoadData(customer);
                }
            }

            return list;
        }

        public async Task<IEnumerable<T>> GetWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            var list = await _collection.Find<T>(predicate).ToListAsync();
            foreach (var entity in list)
            {
                if (entity is Customer customer)
                {
                    await LoadData(customer);
                }
            }
            return list;
        }

        public async Task UpdateAsync(T entity)
        {
            await _collection.ReplaceOneAsync(t => t.Id == entity.Id, entity);
        }

        private async Task LoadData(Customer customer)
        {
            if (customer != null)
            {
                if (customer.Preferences?.Count > 0)
                {
                    var ids = customer.Preferences.Select(c => c.PreferenceId);

                    var preferencies = await _preferenceCollection
                        .Find(e => ids.Contains(e.Id)).ToListAsync();
                    if (preferencies.Count > 0)
                    {
                        foreach (var preference in customer.Preferences)
                        {
                            preference.Customer = customer;
                            preference.CustomerId = customer.Id;
                            preference.Preference =
                                preferencies.FirstOrDefault(c => preference.PreferenceId == c.Id);
                        }
                    }
                }

                if (customer.PromoCodes?.Count > 0)
                {
                    var ids = customer.PromoCodes?.Select(c => c.PromoCodeId);

                    var promocodes = await _promocodeCollection
                        .Find(e => ids.Contains(e.Id)).ToListAsync();
                    if (promocodes.Count > 0)
                    {
                        foreach (var promoCode in customer.PromoCodes)
                        {
                            promoCode.Customer = customer;
                            promoCode.CustomerId = customer.Id;
                            promoCode.PromoCode = promocodes.FirstOrDefault(c => promoCode.PromoCodeId == c.Id);
                        }
                    }
                }
            }
        }
    }
}
