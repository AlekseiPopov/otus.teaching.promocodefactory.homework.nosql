﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public static class FakeDataFactory
    {
        private static Guid _customerId1 = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
        private static Guid _customerId2 = Guid.Parse("44755c34-8193-4ae2-af9d-e31bc833328a");


        private static Guid _preference1 = Guid.Parse("7502a3ed-7b53-441e-a64e-9c2c634abb9e");
        private static Guid _preference2 = Guid.Parse("36266dbf-52e6-4816-9534-888186bc43e6");
        private static Guid _preference3 = Guid.Parse("2e18e7ce-b586-46d7-bf4f-e5bd7551011e");

        private static Guid _promoCode1 = Guid.Parse("b14d3bc5-5e9b-4444-8161-fcd5cfca54bf");
        private static Guid _promoCode2 = Guid.Parse("88ffe51f-56ca-479d-ab0f-6e21cd7b3d02");


        public static List<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = _preference1,
                Name = "Театр",
            },
            new Preference()
            {
                Id = _preference2,
                Name = "Семья",
            },
            new Preference()
            {
                Id = _preference3,
                Name = "Дети",
            }
        };

        public static List<Customer> Customers
        {
            get
            {
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = _customerId1,
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        Preferences = new List<CustomerPreference>()
                        {
                            new CustomerPreference()
                            {
                                CustomerId = _customerId1,
                                PreferenceId = _preference1
                            },
                            new CustomerPreference()
                            {
                                CustomerId = _customerId1,
                                PreferenceId = _preference2
                            }
                        },
                        PromoCodes = new List<PromoCodeCustomer>()
                        {
                            new PromoCodeCustomer()
                            {
                                CustomerId = _customerId1,
                                PromoCodeId = _promoCode1
                            },
                            new PromoCodeCustomer()
                            {
                                CustomerId = _customerId1,
                                PromoCodeId = _promoCode2
                            }
                        }
                    },
                    new Customer()
                    {
                        Id = _customerId2,
                        Email = "sergey_sergeyev@mail.ru",
                        FirstName = "Сергей",
                        LastName = "Сергеев",
                        Preferences = new List<CustomerPreference>()
                        {
                            new CustomerPreference()
                            {
                                CustomerId = _customerId2,
                                PreferenceId = _preference1
                            },
                            new CustomerPreference()
                            {
                                CustomerId = _customerId2,
                                PreferenceId = _preference3
                            }
                        },
                        PromoCodes = new List<PromoCodeCustomer>()
                        {
                            new PromoCodeCustomer()
                            {
                                CustomerId = _customerId1,
                                PromoCodeId = _promoCode1
                            }
                        }
                    }
                };

                return customers;
            }
        }

        public static List<PromoCode> PromoCodes
        {
            get
            {
                var promoCodes = new List<PromoCode>()
                {
                    new PromoCode()
                    {
                        Id = _promoCode1,
                        Code = "код 1",
                        BeginDate = DateTime.Now.AddDays(3),
                        EndDate = DateTime.Now.AddDays(4),
                        ServiceInfo = "Info 1",
                        PartnerId = Guid.NewGuid(),
                        PreferenceId = _preference1
                    },

                    new PromoCode()
                    {
                        Id = _promoCode2,
                        Code = "код 2",
                        BeginDate = DateTime.Now.AddDays(5),
                        EndDate = DateTime.Now.AddDays(6),
                        ServiceInfo = "Info 2",
                        PartnerId = Guid.NewGuid(),
                        PreferenceId = _preference2
                    }
                };
                return promoCodes;
            }
        }
    }
}