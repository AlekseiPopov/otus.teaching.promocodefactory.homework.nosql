﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IMongoCollection<Customer> _customersCollection;
        private readonly IMongoCollection<Preference> _preferencesCollection;
        private readonly IMongoCollection<PromoCode> _promoCodesCollection;

        public MongoDbInitializer(IMongoContext settings)
        {
            
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _customersCollection = database.GetCollection<Customer>("Customers");
            _preferencesCollection = database.GetCollection<Preference>("Preferences");
            _promoCodesCollection = database.GetCollection<PromoCode>("PromoCodes");

        }

        public void InitializeDb()
        {
            _preferencesCollection.InsertMany(FakeDataFactory.Preferences);
            _customersCollection.InsertMany(FakeDataFactory.Customers);
            _promoCodesCollection.InsertMany(FakeDataFactory.PromoCodes);
        }

        public async void RecreateDb()
        {
            await _preferencesCollection.Database.DropCollectionAsync("Customers");
            await _customersCollection.Database.DropCollectionAsync("Preferences");
            await _promoCodesCollection.Database.DropCollectionAsync("Promocodes");
            await _preferencesCollection.InsertManyAsync(FakeDataFactory.Preferences);
            await _customersCollection.InsertManyAsync(FakeDataFactory.Customers);
            await _promoCodesCollection.InsertManyAsync(FakeDataFactory.PromoCodes);
        }
    }
}